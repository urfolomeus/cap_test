# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'cap_test'

set :repo_url, 'git@bitbucket.org:urfolomeus/cap_test.git'
set :ssh_options, forward_agent: true

set :linked_files, fetch(:linked_files, []).push('.ruby-env')
